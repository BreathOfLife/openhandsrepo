export class Charity {
    constructor(
        public charityName: string,
        public orgDetails: OrgDetails,
        public donationAmount?: number,
        public stockOptionValue?: number,
        public stockOptionName?: string) {}
}

export class OrgDetails {
    constructor(public id: number, 
        public category: string,
        public ein: string,
        public missionStatement: string,
        public website: string) {}
}
  
  export interface ICharityResponse {
    total: number;
    results: Charity[];
  }