import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainWindowComponent } from './shared/main-window/main-window.component';
import { PageHeaderComponent } from './shared/page-header/page-header.component';
import { PageBodyComponent } from './shared/page-body/page-body.component';
import { PageFooterComponent } from './shared/page-footer/page-footer.component';
import { SearchComponent } from './shared/search/search.component';
import { CharityComponent } from './shared/charity/charity.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule} from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientInMemoryWebApiModule, InMemoryBackendConfigArgs } from 'angular-in-memory-web-api';
import { InMemDataService } from './services/in-memory-data.service';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AngularMaterialModule } from './angular-material.module';
import { CharityService } from './services/charity.service';
import { ModalComponent } from './shared/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    MainWindowComponent,
    PageHeaderComponent,
    PageBodyComponent,
    PageFooterComponent,
    SearchComponent,
    CharityComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    environment.production ?
    []: HttpClientInMemoryWebApiModule.forRoot(InMemDataService, {delay: 1000, dataEncapsulation: false } as InMemoryBackendConfigArgs) as any
  ],
  providers: [InMemDataService, CharityService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
