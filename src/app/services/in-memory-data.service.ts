import { InMemoryDbService } from 'angular-in-memory-web-api';
import exampleData from '../../../sharedResources/SearchByName.json';
export class InMemDataService implements InMemoryDbService {
  createDb() {
    // let charities = [
    //   { id: 1, name: 'Windstorm' },
    //   { id: 2, name: 'Bombasto' },
    //   { id: 3, name: 'Magneta' },
    //   { id: 4, name: 'Tornado' },
    //   { id: 5, name: 'Agnosto' }
    // ];
    let charities = exampleData.content;
    return {charities: {
      total: charities.length,
      results: charities
    }};
  }
}