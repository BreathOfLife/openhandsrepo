import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {Charity, ICharityResponse} from '../models/charity.class'

@Injectable()
export class CharityService {
  
  inputReady() {
    return new Promise((res, rej) => {
        this.newInputValueEmitter.subscribe((value) => {
            this.newInput = value; 
            res(true);
        })
    });
  }

  newInput: any;

  charityEvent = new EventEmitter<Charity>();

  editDonation = new EventEmitter<Charity>();

  deleteDonation = new EventEmitter<Charity>();

  okButtonClicked = new EventEmitter<boolean>();

  newInputValueEmitter = new EventEmitter<any>();

  hideGrabberModal = new EventEmitter<boolean>();

  hideGrabberModalComplete = new EventEmitter<boolean>();

  constructor(private http: HttpClient) {}

  search(filter: {name: string} = {name: ''}, page = 1): Observable<ICharityResponse> {
    return this.http.get<ICharityResponse>('/api/charities')
    .pipe(
      tap((response: ICharityResponse) => {
        response.results = response.results 
          .map(charity => new Charity(charity.charityName, charity.orgDetails))
          // Not filtering in the server since in-memory-web-api has somewhat restricted api
          .filter(charity => {
              if (filter.name === '') {
                  return false;
              } else {
                return charity.charityName.toLowerCase().includes(filter.name.toLowerCase())
              }
          })

        return response;
      })
      );
  }
}
