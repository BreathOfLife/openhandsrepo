import { Component, Input, OnInit } from '@angular/core';
import { Charity } from 'src/app/models/charity.class';
import { CharityService } from 'src/app/services/charity.service';

@Component({
  selector: 'charity',
  templateUrl: './charity.component.html',
  styleUrls: ['./charity.component.scss']
})
export class CharityComponent implements OnInit {

  @Input() info: Charity | undefined;

  constructor(private charityService: CharityService) { }

  ngOnInit(): void {
  }

  editDonation() {
    this.charityService.editDonation.emit(this.info);
  }

  deleteDonation() {
    this.charityService.deleteDonation.emit(this.info);
  }

}
