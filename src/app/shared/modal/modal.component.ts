import { OnChanges, OnDestroy, Output, SimpleChanges } from '@angular/core';
import { Component, ElementRef, EventEmitter, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CharityService } from 'src/app/services/charity.service';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnChanges, OnDestroy {

  @Input() actionItem: any = {};
  @Input() show: boolean = false;
  @Output() showChange = new EventEmitter<boolean>();
  eventCompleted: boolean = false;
  _hidden = true;
  _show = false;

  ele: HTMLElement | undefined;

  subscriptionList: Subscription[] = [];

  constructor(private elementRef: ElementRef, private charityService: CharityService) { }

  

  ngOnChanges(changes: SimpleChanges): void {

    for (let change in changes) {
      if (change === 'show') {
        this.show = changes[change].currentValue;
        this._hidden = !this.show; 
          setTimeout(() => {
            this._show = this.show;
          }, 50);
      }
    }
  }

  ngOnInit(): void {
    let sub = this.charityService.okButtonClicked.subscribe((value: any) => {
      this.hideInputModal();
    });
    this.subscriptionList.push(sub);
    
    
  }
  

  hideInputModal() { 
    setTimeout(() => {
      this.show = false;
      this.showChange.emit(false);
      setTimeout(() => {
        this._hidden = true;
        this.charityService.hideGrabberModalComplete.emit(true);
      },200);
    },1);
  }

  ngOnDestroy() {
    this.subscriptionList.forEach((value: Subscription) => {
      value.unsubscribe();
    })
  }

}
