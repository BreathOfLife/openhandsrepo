import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Charity } from 'src/app/models/charity.class';
import { CharityService } from 'src/app/services/charity.service';

@Component({
  selector: 'page-body',
  templateUrl: './page-body.component.html',
  styleUrls: ['./page-body.component.scss']
})
export class PageBodyComponent implements OnInit {

  charities: Charity[] = [];

  totalAmount = '$0.00';

  showInput: boolean = false; 

  showInputYesNo: boolean = false; 

  showPayment: boolean = false; 

  showConfirmation: boolean = false;

  newValue = '';

  modalTitle = '';

  showCardFillSection = false;
  
  sub: Subscription[] = [];

  constructor(private charityService: CharityService) { }

  isValueInCharities(value: Charity) {
    let found = this.charities.findIndex((val: Charity) => {
       return val.charityName === value.charityName;
    });
    return found != -1;
  }

  updateInputValue() {
    this.charityService.newInputValueEmitter.emit(this.newValue);
    this.charityService.okButtonClicked.emit(true);
    this.newValue = '';
  }

  ngOnInit(): void {

    this.charityService.charityEvent.subscribe(async (value: Charity) => {

      if (!this.isValueInCharities(value)) {

        let valueToUpdate = 0;

        this.modalTitle = `Please, how much do you want to donate to ${value.charityName}?`;

        this.showInput = true;
  
        //let donationAmount = prompt('Please, how much do you want to donate?');
  
        await this.charityService.inputReady();

        let donationAmount = this.charityService.newInput;

        let newP = new Promise<void>((res, rej) => {

          setTimeout(async () => {

            this.modalTitle = `We notice you have some stocks here with us at Citi, please would you like to donate some of them as well to ${value.charityName}?`
    
            this.showInputYesNo = true;
    
            await this.charityService.inputReady();
    
            let yesno = this.charityService.newInput;
    
            if (yesno) {

              new Promise((res, rej) => {
                setTimeout(async () => {

                  this.modalTitle = `Please, how much stock options do you want to donate to ${value.charityName}?`;
    
                  this.showInput = true;
            
                  await this.charityService.inputReady();

                  value.stockOptionValue = this.charityService.newInput;

                  res(true);
                }, 500)
              }).then();

    
            }
            
            if (donationAmount) {
              valueToUpdate = +donationAmount;
              if (isNaN(valueToUpdate)) {
                valueToUpdate = 0;
              }
            }
            value.donationAmount = valueToUpdate;
    
            this.charities.push(value);
          
            this.updateTotalAmountValue();

            res();

          }, 400);

        });

        newP.then();

      }
  
    });

    this.charityService.editDonation.subscribe(async (value: Charity) => {

      let found = this.charities.findIndex((val: Charity) => {
        return val.charityName === value.charityName;
      });
      let valueToUpdate = 0;

      this.modalTitle = `Please, how much do you want to donate to ${value.charityName}?`;

      this.showInput = true;

      //let donationAmount = prompt('Please, how much do you want to donate?');

      await this.charityService.inputReady();

      let donationAmount = this.charityService.newInput;

      if (donationAmount) {
        valueToUpdate = +donationAmount;
        if (isNaN(valueToUpdate)) {
          valueToUpdate = 0;
        }
      }
      this.charities[found].donationAmount = valueToUpdate;
      this.updateTotalAmountValue();
    });

     this.charityService.deleteDonation.subscribe((value: Charity) => {
      let found = this.charities.findIndex((val: Charity) => {
        return val.charityName === value.charityName;
     });
     this.charities.splice(found, 1);
    })
  }

  updateInputYesNo(pick: string) {
    this.charityService.newInputValueEmitter.emit(pick === 'yes');
    this.charityService.okButtonClicked.emit(true);
    this.newValue = '';
  }

  makeAPayment(){
    this.showPayment = true; 
  }

  cancelDonation() {
    this.charities = [];
  }

  submitPaymentChoice() {
    this.charityService.okButtonClicked.emit(true);
    setTimeout(() => {
      this.showConfirmation = true;
      this.charities = [];
    }, 1000);
  }

  closeInput() {
    this.charityService.okButtonClicked.emit(true);
  }

  onItemChange(value: any) {

    let selectedValue =  value?.target?.value;
    
    this.showCardFillSection = selectedValue === 'other';
    

  }

  updateTotalAmountValue() {
      let total = 0.00;
      this.charities.forEach((charity: Charity) => {
        let donAmount = 0;
        if (charity.donationAmount) {
          donAmount =  charity?.donationAmount;
        }
        total = total + donAmount;
      });
      this.totalAmount = `$${total}`;
  }

}
