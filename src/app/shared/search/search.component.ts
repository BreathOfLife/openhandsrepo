import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { Charity } from 'src/app/models/charity.class';
import { CharityService } from 'src/app/services/charity.service';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  filteredCharities: Charity[] = [];

  userInput = new FormControl();

  isLoading = false;

  hideDropDown = true; 

  constructor(private charityService: CharityService) {}

  ngOnInit() {

      this.userInput.valueChanges
      .pipe(
        debounceTime(300),
        tap(() => this.isLoading = true),
        switchMap(value => this.charityService.search({name: value}, 1)
        .pipe(
          finalize(() => this.isLoading = false),
          )
        )
      )
      .subscribe((charities: any) => {
        this.hideDropDown = charities.results.length == 0;
        this.filteredCharities = charities.results
      });
  }

  addCharityToDonationList(charity: Charity) {
    this.hideDropDown = true;
    this.userInput.setValue('');
    this.charityService.charityEvent.emit(charity);
  }

}
